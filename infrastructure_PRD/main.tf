terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.2.0"
    }
  }
  required_version = ">= 1.2.0"
}

variable "ubuntu_ami" {
  description = "The AMI to use for the server"
  default     = "ami-0c7217cdde317cfec"
  type        = string

}

variable "free_tier_instance_type" {
  description = "The instance type to use for the server"
  default     = "t2.small"
  type        = string

}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh_PRD"
  description = "Allow SSH and 80 inbound traffic"

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Access to instance 8080"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Access to instance 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

provider "aws" {
  region = "us-east-1"
}
resource "aws_key_pair" "deployer" {
  key_name   = "deployer_PRD"
  public_key = file("my-temp-key.pub")
}
resource "aws_instance" "test_server" {
  ami             = var.ubuntu_ami
  instance_type   = var.free_tier_instance_type
  key_name        = "deployer_PRD"
  security_groups = [aws_security_group.allow_ssh.name]
  user_data       = file("docker.sh")
  tags = {
    Name = "test_server_PRD"
  }
}

output "aws_instance_public_ip" {
  value = aws_instance.test_server.public_ip
}
