terraform {
  backend "s3" {
        bucket = "tfstateqa"
        key    = "terraform.tfstate"
        region = "us-east-1"
    }
}